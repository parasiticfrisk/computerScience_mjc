# Modesto Junior College - Computer Science
* [x] ![CSCI 270]() - Introduction to Programming (Python)
* [x] ![CSCI 210]() - Introduction to UNIX/Linux
* [x] ![CSCI 269]() - Networking Devices
* [ ] ![CSCI 271]() - Problem Solving and Programming 1 (coming soon...)
* [ ] ![CSCI 272]() - Problem Solving and Programming 2 (coming soon...)
* [ ] ![CSCI 273]() - Assembly Language Programming (coming soon...)

# Introduction to UNIX/Linux Systems (CSCI 210)
Bash scripts and projects from my Introduction to UNIX/Linux course at MJC. I plan on continuing to come back to these scripts to change them in various ways or use as a reference.

#### Course Description:
Introduction to the UNIX operating system using Linux. Coverage will include using UNIX shells, commands, the role of the system administrator, the UNIX file system, editors, file processing, shell programming utilities, PERL and CGI programming, C and C++ programming, and recent developments in UNIX and the Windows graphical user interface. Extensive hands-on experience using UNIX operating system and programming within the UNIX environment.

# Final Project
### Project Details:
Installation of Pi-hole combined with OpenVPN server for secure access from remote clients. From this VPN:
* Use DNS server and full filtering capabilities Pi-hole from everywhere with internet access
* Access the admin interface remotely
* Encrypt Internet traffic

### Additional Features:
* Add an “indicator” LED to show when an ad is blocked
* Build a custom housing for the Pi-hole to *look like a book*
* Enable SSH for easy access from anywhere

#### [Install Pi-hole + OpenVPN](https://github.com/pi-hole/pi-hole/wiki/OpenVPN-server:-Installation)
#### [Configure OpenVPN to use Pi-hole as DNS](https://github.com/pi-hole/pi-hole/wiki/OpenVPN-server:-Setup-OpenVPN-server)
#### [Configure Clients: Android](https://github.com/pi-hole/pi-hole/wiki/OpenVPN-server:-Connect-from-a-client-(Android))
#### [Firewall Configuration](https://github.com/pi-hole/pi-hole/wiki/OpenVPN-server:-Firewall-configuration-(using-iptables))
#### [Dual Server: LAN & VPN](https://github.com/pi-hole/pi-hole/wiki/OpenVPN-server:-Dual-operation:-LAN-&-VPN-at-the-same-time)

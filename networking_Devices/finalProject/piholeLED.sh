#!/bin/bash
#
#    Monitor Pi-Hole log and turn on an LED connected to header pin 12 (gpio18)
#    when the "gravity.list" block string is detected in the pi-hole log. 
#    To modify, change gpio[n] to the corresponding pin(s) to suit what you want to do 
#    (echo out to the terminal, send an email, display text on an LCD screen, 
#    play music over a speaker...)
#
#    August Frisk | © 2018
#
#    Enable control of header pin 12 (gpio18) as an output
echo "18" > /sys/class/gpio/export
echo "out" > /sys/class/gpio/gpio18/direction
#
#    Turn the pin on or off with a varying wait time in between.
#
startupBlink () {
    echo "1" > /sys/class/gpio/gpio18/value
    sleep 0.75
    echo "0" > /sys/class/gpio/gpio18/value
    sleep 0.25
}
piholeBlink () {
    echo "1" > /sys/class/gpio/gpio18/value
    sleep 0.25
    echo "0" > /sys/class/gpio/gpio18/value
    sleep 0.125
}
#
#    Script startup visual indicator: Blink 5 times
for i in {0..4..1}
do
    startupBlink
done
#
#    Watch the pihole.log file for gravity string and blink LED to indicate a block
tailf /var/log/pihole.log | while read INPUT
do
    if [[ "$INPUT" == *"/etc/pihole/gravity.list"* ]]; 
    then
        piholeBlink
    fi
done

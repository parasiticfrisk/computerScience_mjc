# Networking Devices and Systems Lab (CMPET 269)

This laboratory course is designed give practical hands-on experience with a variety of computer network technologies by researching, designing, and building a network application (“app”).

**Introduction** - Learn Python Programming Language

**Lab 01** - Controlling Digital Outputs and Sensing Digital Inputs

**Lab 02** - Analog Instrumentation

**Lab 03** - Remotely Accessing a Computer Over a Network

**Lab 04** - Building a Web Server

**Lab 05** - A Messaging Intruder Alarm

**Lab 06** - An Active Web Server for the Internet of Things

**Lab 07** - Internet of Things Device Control with If-This-Then-That

**lab 08** - Internet Control and Monitoring using MQTT

**lab 09** - Building a Network Attached Storage

**final project** - Coming Soon...

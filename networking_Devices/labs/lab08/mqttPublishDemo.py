#!/usr/bin/env python3
'''
CMPET 269 Raspberry Pi Labs
Lab 08 Link:
https://electricworld.us/moodle/mod/quiz/view.php?id=636
'''
# MQTT Publish Demo
# Publish two messages, to two different topics

import paho.mqtt.publish as publish

publish.single("CoreElectronics/test", "Hello", hostname="test.mosquitto.org")
publish.single("CoreElectronics/topic", "World!", hostname="test.mosquitto.org")
print("Done")

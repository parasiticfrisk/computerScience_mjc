# Lab 05

#### Goal: [Build Your Own LAMP Web Server on Your Raspberry Pi with WordPress](https://projects.raspberrypi.org/en/projects/lamp-web-server-with-wordpress)
**What you will Learn**
* Install software on your Raspberry Pi
* Install and configure Apache, PHP, and MySQL to create a LAMP web server
* Download WordPress and run it as a local website on your Raspberry Pi
* Configure WordPress and make your website accessible to other devices on your local network

**LAMP** stands for **L**inux, **A**pache, **M**ySQL, **P**HP. It is an open source Web development platform that uses Linux as the operating system, Apache as the Web server, MySQL as the relational database management system and PHP as the object-oriented scripting language. (Sometimes Perl or Python is used instead of PHP.) This website (electricworld.us) runs a LAMP server.

**Apache** is a popular web server application you can install on a computer such as the Raspberry Pi to allow it to serve web pages. On its own, Apache can serve web pages written as HTML (Hypertext Markup Language) files over HTTP (Hypertext Transfer Protocol) also known as the "web". On its own, Apache can serve HTML files over HTTP, and with additional modules can serve dynamic web pages using scripting languages such as PHP.

**PHP** is a scripting (programming) language that allows the Apache web server to serve dynamic (changing) web pages. PHP (recursive acronym for PHP: Hypertext Preprocessor) is a widely-used open source general-purpose scripting (programming) language that is especially suited for web development and can be embedded into HTML.

**MySQL** allows a LAMP server to host web based applications that require a database. MySQL (officially pronounced as "My S-Q-L",) is an open-source relational database management system (RDBMS). "SQL", is the the abbreviation for Structured Query Language.

**WordPress** is a free and open-source Content Management System (CMS) used to create and maintain websites. WordPress is designed to be installed on a web server which runs the PHP server scripting language and the MySQL server. It can be installed on the LAMP server.

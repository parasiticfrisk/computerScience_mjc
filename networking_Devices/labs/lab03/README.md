
# Lab 3

### Goal: Reading Analog Sensors On The Raspberry Pi Using An MCP3008 Analog to Digital Converter (ADC)
**Parts Required:**
* Raspberry Pi
* MCP3008
* Light Dependant Resistor (LDR)
* Thermistor
* 1k Ohm resistor
* 10k Ohm resistor
* Breadboard
* Jumper wires with male-male and male-female connectors

#### Circuit:
![](https://www.mathworks.com/help/examples/raspberrypiio_product/win64/mcp3008_circuit.png)

#### Enable SPI Interface
To enable hardware SPI on the Pi, make the following modification to the system file:
```
sudo nano /boot/config.txt
```
Add the line:
```
dtparam=spi=on
```
Press ```ctrl+x```, ```y```, then ```enter / return``` to save and exit the file. Reboot using:
```
sudo reboot
```
##### Check if SPI is enabled
```
lsmod
```
You should see ```spi_bcm2708``` listed in the output. To make it easier to spot:
```
lsmod | grep spi_
```
#### Install Python Library
Install the ```py-spidev``` library to read data from the SPI bus in Python
```
sudo apt-get install python2.7-dev
wget https://github.com/Gadgetoid/py-spydev-archive/master.zip
unzip master.zip
rm master.zip
cd py-spydev-master
sudo python setup.py install
cd ..
```

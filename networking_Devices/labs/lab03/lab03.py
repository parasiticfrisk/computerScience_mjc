#!/usr/bin/python
'''
CMPET 269 Raspberry Pi Labs
Lab 3 link:
https://electricworld.us/moodle/mod/quiz/view.php?id=430

Python program to read temperature and photon flux (light intensity) as detected by thermistor and photocell transducers.
Uses Microchip corporation MCP3008 8 channel, 10-bit Analog to Digital Converter (ADC).
Uses ADC channel 0 for light, and channel 1 for temperature.
The following list shows how the MCP3008 can be connected. It requires 4 GPIO pins on the Pi P1 Header.
'''

# VDD   3.3V
# VREF  3.3V
# AGND  GROUND
# CLK   GPIO11 (P1-23)
# DOUT  GPIO9  (P1-21)
# DIN   GPIO10 (P1-19)
# CS    GPIO8  (P1-24)
# DGND  GROUND

# The CH0-CH7 pins are the 8 analog inputs.

import spidev
import time
import os
import math

# Define sensor channels
light_channel = 0
temp_channel  = 1
 
# Define delay between readings
delay = 0.5
 
# Open SPI bus
spi = spidev.SpiDev()
spi.open(0,0)
 
# Function to read SPI data from MCP3008 chip
# Channel must be an integer 0-7
def ReadChannel(channel):
  adc = spi.xfer2([1,(8+channel)<<4,0])
  data = ((adc[1]&3) << 8) + adc[2]
  return data
 
# Function to convert data to voltage level,
# rounded to specified number of decimal places.
def ConvertVolts(data,places):
  volts = (data * 3.3) / float(1023)
  volts = round(volts,places)
  return volts

'''
Function to calculate temperature from from Analog to Digital Converter (ADC) Value
from thermistor data, converted with the Steinhart Hart Equation and rounded to specified
number of decimal places.
'''
def ConvertTemp(value,places):
    volts = (value * 3.3) / 1024 #calculate the voltage

    if volts > 0.0: # Prevent divide by zero error in case ADC returns zero. 
      ohms = ((1/volts)*3300)-1000 #calculate the ohms of the thermistor
    else:
      ohms = 0.0
      print
      print ("Error, ADC chip is returning zero. Check the SPI connections to the MCP3008 chip.")
      print
      
    lnohm = math.log1p(ohms) #take ln(ohms)
    #a, b, & c values from http://www.thermistor.com/calculators.php
    #using curve R (-6.2%/C @ 25C) Mil Ratio X
    a =  0.002197222470870
    b =  0.000161097632222
    c =  0.000000125008328
 
    #Steinhart Hart Equation
    # T = 1/(a + b[ln(ohm)] + c[ln(ohm)]^3)
 
    t1 = (b*lnohm) # b[ln(ohm)]
 
    c2 = c*lnohm # c[ln(ohm)]
 
    t2 = math.pow(c2,3) # c[ln(ohm)]^3

    temp = 1/(a + t1 + t2) #calculate temperature in degrees Kelvin
 
    tempc = temp - 273.15 - 4 #Convert degrees Kelvin to Celsius. (The -4 is error correction for bad python math.)

    tempc = round(tempc,places)
    return tempc

 
while True:
 
  # Read the light sensor data
  light_level = ReadChannel(light_channel)
  light_volts = ConvertVolts(light_level,2)
 
  # Read the temperature sensor data
  temp_level = ReadChannel(temp_channel)  # Get raw ADC value for temperature
  temp_volts = ConvertVolts(temp_level,2) # Convert to voltage
  temp_C     = ConvertTemp(temp_level,2)  # Convert to degrees Celsius
  temp_F     = temp_C*9/5+32              # Convert to degrees Fahrenheit
  
  # Print out results
  print "--------------------------------------------"
  print("Light Level: ADC={} Voltage={}V".format(light_level,light_volts))
  print("Temperature: ADC={} Voltage={}V {} deg C {} deg F".format(temp_level,temp_volts,temp_C,temp_F))
 
  # Wait before repeating loop
  time.sleep(delay)

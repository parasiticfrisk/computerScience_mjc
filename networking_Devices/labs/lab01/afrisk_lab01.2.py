#!/usr/bin/env python3
'''
CMPET 269 Raspberry Pi Labs
Lab 1.2 link:
https://electricworld.us/moodle/mod/quiz/view.php?id=418
'''

import RPi.GPIO as GPIO # import GPIO library
import time # import time library. Allows for use of 'sleep'

GPIO.setmode(GPIO.BOARD) # board pin numbering
GPIO.setup(7, GPIO.OUT) # setup GPIO pin 7 to OUT

# define Blink() function
def Blink(numTimes, speed):
    for i in range(0, numTimes): # run loop numTimes
        print("Iteration " + str(i+1)) # print current loop
        GPIO.output(7, True) # switch on pin 7
        time.sleep(speed) # wait
        GPIO.output(7, False) # switch off pin 7
        time.sleep(speed) # wait
    print("Done") # print done when loop is complete
    GPIO.cleanup()

# Get user input for number and length of blinks
iterations = raw_input("Enter total number of times to blink: ")
speed = raw_input("Enter length of each blink(seconds): ")

# Start Blink() function. Convert input from strings to number data type
# & pass to to Blink() as parameters
Blink(int(iterations),float(speed))

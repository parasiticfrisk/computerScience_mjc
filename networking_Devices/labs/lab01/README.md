# Lab 1

### Goal: Control an LED using your raspberry pi and python
**What you will learn:**
* Construct a basic electrical circuit and attach it to your RPi GPIO pins
* Write a simple Python program to control the circuit using IDLE IDE

#### Controlling the LED with python:
**Here is what our circuit will look like:**

![](http://www.thirdeyevis.com/images/3v-led-schematic.png)

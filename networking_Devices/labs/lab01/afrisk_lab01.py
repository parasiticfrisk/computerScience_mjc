#!/bin/usr/env python3
'''
CMPET 269 Raspberry Pi Labs
Lab 1 link:
https://electricworld.us/moodle/mod/quiz/view.php?id=418
'''

import RPi.GPIO as GPIO # import GPIO library

GPIO.setmode(GPIO.BOARD) # use board pin numbering
GPIO.setup(7, GPIO.OUT) # setup GPIO pin 7 to OUT
GPIO.output(7, True) # turn on GPIO

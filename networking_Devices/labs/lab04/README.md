# Lab 4

### Goal: Connect remotely to control the Raspberry Pi
**What you will learn**
* How to remote control your Raspberry Pi using SSH
* How to install and use VNC on your Raspberry Pi

[SSH (Secure Shell) Option](https://learn.adafruit.com/adafruits-raspberry-pi-lesson-6-using-ssh/overview)

[VNC (Virtual Network Connection) Option](https://learn.adafruit.com/adafruit-raspberry-pi-lesson-7-remote-control-with-vnc/overview)

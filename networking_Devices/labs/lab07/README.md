# Lab 7

### Goal: [Create a Raspberry Pi Fridge Monitor Alarm](https://www.raspberrypi.org/magpi/raspberry-pi-fridge-monitor/)
- [x] Connect the photoresistor
- [x] Start a new recipe in IFTT
- [x] Create a Maker Channel trigger
- [x] Choose an action
- [x] Complete the actions field
- [x] Complete the recipe
- [x] Find your secret key

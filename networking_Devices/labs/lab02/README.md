# Lab 2

### Goal: Connecting a Push Switch with Raspberry Pi

#### Description in [video](https://youtu.be/3TDJ4FmtGgk)

**Lab 02** - Detect Input with Switch

**Lab 02.1** - Create Python version of "Tuxx Crossing"

**Tuxx.c** - C code "Crossing" Game

**Tuxx.sh** - BASH "Crossing" Game

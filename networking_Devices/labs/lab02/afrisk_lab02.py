#!/usr/bin/env python

'''
CMPET 269 Raspberty Pi Labs
Lab 2 Link:
https://electricworld.us/moodle/mod/quiz/view.php?id=417
'''

import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)

GPIO.setup(18, GPIO.IN, pull_up_down=GPIO.PUD_UP)

while True:
    input_state = GPIO.input(18)
    if input_state == False:
        print('Button Pressed')
        time.sleep(0.2)

#!/usr/bin/env python
'''
CMPET 269 Raspberry Pi Labs
Lab 2.1 Link:
https://electricworld.us/moodle/mod/quiz/view.php?id=419
'''

from gpiozero import Button, LED, TrafficLights, Buzzer
from time import sleep

# varibles for "traffic signal"
button = Button(21)
green = LED(2)
red = LED(3)
lights = TrafficLights(25, 8, 7)
buzzer = Buzzer(15)

# pedestrian crossing at traffic light
while True:
    print("Waiting for button...")
    lights.green.on()
    red.on()
    button.wait_for_press()
    print("Please wait...")
    # stop traffic:
    # cycle lights from Green to Red
    sleep(5)
    print("Stopping traffic...")
    lights.green.off()
    lights.amber.on()
    sleep(5)
    red.off()
    print("Please cross")
    # signal cross walk
    green.on()
    lights.amber.off()
    lights.red.on()
    sleep(5)
    # start grace period
    # flashing lights / buzzer
    print("Tic toc!")
    for i in range(5, 0, -1):
        sleep(1)
        green.blink()
        lights.amber.blink()
        buzzer.beep()
        print(i)
        # start traffic
    print("Starting traffic...")
    lights.off()
    green.off()
    buzzer.off()
    red.on()
    lights.green.on()

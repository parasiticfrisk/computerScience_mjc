# Lab 09

#### Goal: [Install ownCloud 10](https://www.avoiderrors.net/owncloud-10-raspberry-pi-3-raspbian-stretch/) on the Raspberry Pi
**What you will learn:**
* Update Raspberry Pi System
* Install LAMP Server
* Install ownCloud dependancies
* Install ownCloud 10
* Mount an external hard drive for larger storage capacity
* Enable SSL for secure access
* OwnCloud configuration via web browser

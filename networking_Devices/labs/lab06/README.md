# Lab 06 Part 1

### Goal: [Build a Python Web Server with Flask](https://projects.raspberrypi.org/en/projects/python-web-server-with-flask)
**What you will learn**
* How to install software on your Raspberry Pi
* How to install pip and Flask to create a Python-powered web server
* How to build a basic web app with Flask and run it as a local website on your Raspberry Pi
* How routes are used to map URLs to web pages
* How to use HTML to create simple web page templates
* How to use CSS to control the appearance of HTML content
* How to configure Flask and make your website accessible to other devices on your local network

# Lab 06 Part 2
### Goal: [Python Web Server with Flask and Raspberry Pi](http://www.instructables.com/id/Python-WebServer-With-Flask-and-Raspberry-Pi/)

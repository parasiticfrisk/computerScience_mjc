# [Learn Python](https://www.learnpython.org/) by Example

### The basics:
- [x] Hello, World!
- [x] Variables and Types
- [x] Lists
- [x] Basic Opertators
- [x] String Formatting
- [x] Basic String Operations
- [x] Conditions
- [x] Loops
- [x] If/Else statements
- [x] Lists
- [x] Functions
- [x] Classes and Objects
- [x] Dictionaries
- [x] Modules and Packages

### Data Science:
- [x] Numpy Arrays
- [x] Pandas Basics

### Advanced:
- [x] Generators
- [x] List Comprehensions
- [x] Multiple Function Arguments
- [x] Regular Expressions
- [x] Exception Handling
- [x] Sets
- [x] Serialization
- [x] Partial Functions
- [x] Code Introspection
- [x] Closures
- [x] Decorators

#!/usr/bin/env python3
'''
CMPET 269 Networking Devices
learn Python by practice
https://www.learnpython.org
'''


# Start section: Sets
print(set("my name is Eric and Eric is my name".split()))


a = set(["Jake", "John", "Eric"])
print(a)
b = set(["John", "Jill"])
print(b)


a = set(["Jake", "John", "Eric"])
b = set(["John", "Jill"])

print(a.intersection(b))
print(b.intersection(a))


a = set(["Jake", "John", "Eric"])
b = set(["John", "Jill"])

print(a.symmetric_difference(b))
print(b.symmetric_difference(a))


a = set(["Jake", "John", "Eric"])
b = set(["John", "Jill"])

print(a.difference(b))
print(b.difference(a))


a = set(["Jake", "John", "Eric"])
b = set(["John", "Jill"])

print(a.union(b))


# Excercise
'''
Use the given lists to print out a set containing all the participants from event A which did not attend event B.
'''
a = ["Jake", "John", "Eric"]
b = ["John", "Jill"]

A = set(a)
B = set(b)

print(A.difference(B))
# End section: Sets

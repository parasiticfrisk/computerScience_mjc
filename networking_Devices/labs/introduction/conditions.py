#!/usr/bin/env python3
'''
CMPET 269 Networking Devices
learn Python by practice
https://www.learnpython.org
'''

# start section: Conditions
x = 2
print(x == 2) # prints out True
print(x == 3) # prints out False
print(x < 3) # prints out True

# Boolean operators
name = "John"
age = 23
if name == "John" and age == 23:
    print("Your name is John, and you are also 23 years old.")

if name == "John" or name == "Rick":
    print("Your name is either John or Rick.")

# The 'in' operator
name = "John"
if name in ["John", "Rick"]:
    print("Your name is either John or Rick.")

x = 2
if x == 2:
    print("x equals two!")
else:
    print("x does not equal to two.")

# The 'is' operator
x = [1,2,3]
y = [1,2,3]
print(x == y) # Prints out True
print(x is y) # Prints out False

# The 'not' operator
print(not False) # Prints out True
print((not False) == (False)) # Prints out False

# Exercise
'''
Change the variables in the first section, so that each if statement
resolves as True.
'''
# change this code
number = 16
second_number = 0
first_array = [1,2,3]
second_array = [1,2]

if number > 15:
    print("1")

if first_array:
    print("2")

if len(second_array) == 2:
    print("3")

if len(first_array) + len(second_array) == 5:
    print("4")

if first_array and first_array[0] == 1:
    print("5")

if not second_number:
    print("6")
# end section: Conditions

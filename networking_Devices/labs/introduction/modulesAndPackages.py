#!/usr/bin/env python3
'''
CMPET 269 Networking Devices
learn Python by practice
https://www.learnpython.org
'''

# start section: Modules and Packages
# import the library
import urllib

# use it
urllib.urlopen(...)

# Exploring built-in modules
'''
>>> import urllib
>>> dir(urllib)
['ContentTooShortError', 'FancyURLopener', 'MAXFTPCACHE', 'URLopener', \
'__all__', '__builtins__', '__doc__', '__file__', '__name__', '__package__', \
'__version__', '_ftperrors', '_get_proxies', '_get_proxy_settings', '_have_ssl'\
, '_hexdig', '_hextochr', '_hostprog', '_is_unicode', '_localhost', '_noheaders'\
, '_nportprog', '_passwdprog', '_portprog', '_queryprog', '_safe_map', \
'_safe_quoters', '_tagprog', '_thishost', '_typeprog', '_urlopener', '_userprog'\
, '_valueprog', 'addbase', 'addclosehook', 'addinfo', 'addinfourl', \
'always_safe', 'basejoin', 'c', 'ftpcache', 'ftperrors', 'ftpwrapper',\
'getproxies', 'getproxies_environment', 'getproxies_macosx_sysconf', 'i',\
'localhost', 'main', 'noheaders', 'os', 'pathname2url', 'proxy_bypass', \
'proxy_bypass_environment', 'proxy_bypass_macosx_sysconf', 'quote', 'quote_plus'\
, 'reporthook', 'socket', 'splitattr', 'splithost', 'splitnport', 'splitpasswd',\
'splitport', 'splitquery', 'splittag', 'splittype', 'splituser', 'splitvalue',\
'ssl', 'string', 'sys', 'test', 'test1', 'thishost', 'time', 'toBytes', \
'unquote', 'unquote_plus', 'unwrap', 'url2pathname', 'urlcleanup', 'urlencode',\
'urlopen', 'urlretrieve']
'''

help(urllib.urlopen)

# Writing modules
# Writing packages
import foo.bar

from foo import bar

__init__.py:

__all__ = ["bar"]

# Exercise
'''
In this exercise, you will need to print an alphabetically sorted list of all
functions in the re module, which contain the word find.
'''
import re

# Your code goes here
find_members = []
for member in dir(re):
    if "find" in member:
        find_members.append(member)

print(sorted(find_members))
# end section: Modules and Packages

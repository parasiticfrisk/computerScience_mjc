#!/usr/bin/env python3
'''
CMPET 269 Networking Devices
learn Python by practice
https://www.learnpython.org
'''

# start section: Hello, World!
print("This line will be printed.")

# Indentation
x = 1
if x ==1:
    # indented four spaces
    print("x is 1.")

# Exercise
# Use the print command to print the line "Hello, World!"
print("Hello, World!")
# end section: Hello, World!

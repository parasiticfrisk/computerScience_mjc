#!/usr/bin/env python3
'''
CMPET 269 Networking Devices
learn Python by practice
https://www.learnpython.org
'''

# start section: Functions
def my_function():
    print("Hello From My Function!")

def my_function_with_args(username, greeting):
    print("Hello, %s , From My Function!, I wish you %s"%(username, greeting))

def sum_two_numbers(a, b):
    return a + b

# How do you call functions in Python?
# Define our 3 functions
def my_function():
    print("Hello From My Function!")

def my_function_with_args(username, greeting):
    print("Hello, %s , From My Function!, I wish you %s"%(username, greeting))

def sum_two_numbers(a, b):
    return a + b

# print(a simple greeting)
my_function()

#prints - "Hello, John Doe, From My Function!, I wish you a great year!"
my_function_with_args("John Doe", "a great year!")

# after this line x will hold the value 3!
x = sum_two_numbers(1,2)

# Exercise
'''
In this exercise you'll use an existing function, and while adding your own to
create a fully functional program.
1. Add a function named list_benefits() that returns the following list of
strings: "More organized code", "More readable code", "Easier code reuse",
"Allowing programmers to share and connect code together"
2. Add a function named build_sentence(info) which receives a single argument
containing a string and returns a sentence starting with the given string and
ending with the string " is a benefit of functions!"
3. Run and see all the functions work together!
'''
# Modify this function to return a list of strings as defined above
def list_benefits():
    return "More organized code", "More readable code", "Easier code reuse", "Allowing programmers to share and connect code together"

# Modify this function to concatenate to each benefit - " is a benefit of functions!"
def build_sentence(benefit):
    return "%s is a benefit of functions!" % benefit


def name_the_benefits_of_functions():
    list_of_benefits = list_benefits()
    for benefit in list_of_benefits:
        print(build_sentence(benefit))

name_the_benefits_of_functions()
# end section: Functions

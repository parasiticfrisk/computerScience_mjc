# Lab 10

### Goal: Add the Chinese Zodiac to your completed Birth Test 

**Suggestions:** 
* Get this working as a standalone program 
* Import the paragraphs 
* Graphical - Add a nice GUI to your completed code 
* submit via canvas as emfp_lab10.py 

#### Chinese Zodiac by Year: 
1. Rat 1900 1912 1924 1936 1948 1960 1972 1984 1996 2008 2020 quick-witted, smart, charming, and persuasive 
2. Ox 1901 1913 1925 1937 1949 1961 1973 1985 1997 2009 2021 patient, kind, stubborn, and conservative 
3. Tiger 1902 1914 1926 1938 1950 1962 1974 1986 1998 2010 2022 authoritative, emotional, courageous, and intense 
4. Rabbit 1903 1915 1927 1939 1951 1963 1975 1987 1999 2011 2023 popular, compassionate, and sincere 
5. Dragon 1904 1916 1928 1940 1952 1964 1976 1988 2000 2012 2024 energetic, fearless, warm-hearted, and charismatic 
6. Snake 1905 1917 1929 1941 1953 1965 1977 1989 2001 2013 2025 charming, gregarious, introverted, generous, and smart 
7. Horse 1906 1918 1930 1942 1954 1966 1978 1990 2002 2014 2026 energetic, independent, impatient, and enjoy traveling 
8. Sheep-Goat 1907 1919 1931 1943 1955 1967 1979 1991 2003 2015 2027 mild-mannered, shy, kind, and peace-loving 
9. Monkey 1908 1920 1932 1944 1956 1968 1980 1992 2004 2016 2028 fun, energetic, and active 
10. Rooster 1909 1921 1933 1945 1957 1969 1981 1993 2005 2017 2029 independent, practical, hard-working, and observant 
11. Dog 1910 1922 1934 1946 1958 1970 1982 1994 2006 2018 2030 patient, diligent, generous, faithful, and kind 
12. Pig 1911 1923 1935 1947 1959 1971 1983 1995 2007 2019 2031 loving, tolerant, honest, and appreciative of luxury 

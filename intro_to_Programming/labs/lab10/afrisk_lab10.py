#!/usr/bin/env python3
#afrisk_lab10.py

import afrisk_Paras as aP
import afrisk_Style as aS
import turtle
import webbrowser

#Personality Test
fname=turtle.textinput('Name','Please enter your name')
bmo=turtle.numinput('Birth Month','Enter your birth month',1,minval=1,maxval=12)
bday=turtle.numinput('Birth Day','Enter your birth day',1,minval=1,maxval=31)
byr=turtle.numinput('Birth Year','Enter your birth year',1997,minval=1900,maxval=2020)

#Calculate Para & CYear Num
def sum1(a):
    return sum([int(i) for i in str(a)])
a=int(bmo+bday+byr)
while a>9:a=sum1(a)
#sum1=bday+byr+bmo
#accum=(sum1%9)
cyear=(a%12)

#Turtle GUI
line1=('Hello! I am '+fname+' and my personality type is '+aP.result[a]+'\n'+'\n'\
       +'My Chinese Zodiac is the '+aP.chyr[cyear])
turtle.write(line1,False,align='center',font=('Arial',12,'bold'))

#Create WebPage
filenameW=('PersonalityTest.html')
fhandle=open(filenameW,'w')
fhandle.write(aS.hdr+aS.css+aS.head+fname+aS.head1+aS.ppara+fname+aS.ppara1+aP.result[a]+aS.zodiac+aP.chyr[cyear]+aS.zodiac1+aS.foot+aS.ftr)
fhandle.close()

#Open WebPage
webbrowser.open_new_tab('PersonalityTest.html')

#Logfile
filename=('afrisk_final.log')
fhandle=open(filename,'a')
fhandle.write(fname+str(bday)+str(bmo)+str(byr)+aP.result[a]+aP.chyr[cyear])
fhandle.close()

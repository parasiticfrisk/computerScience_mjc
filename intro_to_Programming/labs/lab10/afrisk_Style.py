#!/usr/bin/env Python3
#afrisk_Style
#WebPage values:
#HTML Header 
hdr=('<!DOCTYPE html><html><head><title>Personality Test</title>')
#CSS Style
cssHTML=('<style>*{margin: 0;padding: 0;}')
cssbody=('body {font-family: "Handlee", cursive;font-size: 13pt;background-color: #efefef;padding: 10px;margin: 0;}')
cssh1=('h1 {font-size: 15pt;color: #20bcd5;text-align: center;padding: 18px 0 18px 0;margin: 0 0 10px 0;}')
cssh1span=('h1 span {border: 4px dashed #20bcd5;padding: 10px;}')
cssp=('p {padding: 0;margin: 0;}')
cssavatar=('.img-circle {border: 3px solid white;border-radius: 50%;}')
csssection=('.section {background-color: #fff;padding: 15px;margin-bottom: 10px;border-radius: 10px;}')
cssheader=('#header {background-image: url("http://static.guim.co.uk/sys-images/Environment/Pix/columnists/2012/2/7/1328628184434/Carina-Nebula-from-ESOs-V-011.jpg");background-size: cover;}')
cssheaderimg=('#header img {display: block;width: 80px;height: 80px;margin: auto;}')
cssheaderp=('#header p {font-size: 25pt;color: #ffffff;padding-top: 5px;margin: 0;font-weight: bold;text-align: center;}')
cssquote=('.quote {font-size: 12pt;text-align: right;margin-top: 10px;}')
csscopyright=('.copyright {font-size: 8pt;text-align: right;padding-bottom: 10px;color: grey;}')
cssclose=('</style></head><body>')
css=(cssHTML+cssbody+cssh1+cssh1span+cssp+cssavatar+csssection+cssheader+cssheaderimg+cssheaderp+cssquote+csscopyright+cssclose)
#WebPage header
head=('<div id="header" class="section"><img alt="" class="img-circle" src="https://www.iconexperience.com/_img/o_collection_png/green_dark_grey/512x512/plain/astronaut.png"><p>')
head1=('</p></div>')
#Personality Paragraph
ppara=('<div class="section"><h1><span>About Me</span></h1><p>Hello! I am <strong>')
ppara1=('</strong> and I am ')
#Chinese Zodiac
zodiac=('</p><p class="quote">')
zodiac1=('</p></div>')
#WebPage footer
foot=('<div class="copyright"> &copy; 2017 August Frisk. All rights reserved. </div>')
#HTML Footer
ftr=("</body></html>")

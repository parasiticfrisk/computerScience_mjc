# Labs Overview:
### Lab 1: Guess Game
Goal: Key in a known working program and play the game 

### Lab 2: First Sum
Goal: Data input for Birth Test and first calculation

### Lab 3: Coins
Goal: Calculate Dollars and Cents from coins

### Lab 4: Paragraphs
Goal: Selection if/elif/else - Do Not Turn In! 
Goal: Print correct birth test paragraphs 

### Lab 5: Birth Test
Goal: Completed Birth Test

### Lab 6: SQL
Goal: Learn SQL at [w3schools](https://www.w3shcools.com/) 

### Lab 7: Dictionaries
Goal: Refactor completed Birth Test to use a dictionary 

### Lab 8: File i/o
Goal: Refactor completed Birth Test to write to a logfile

### Lab 9: Webpage
Goal: File i/o to create a webpage 

### Lab 10: Turtle GUI
Goal: Add the Chinese Zodiac to your completed Birth Test 

# Lab 1

### Goal: Key in a known working program and play the game.
* Key in the following program into IDLE3 as a new File -> New Window.
* Here is the code you need to type into the editor [guess.py](http://phillipsd.com/270/guess_ok_py.png)

#### What you will learn: 
* How to create/save/modify a program and debug your typos. 
* This will be demonstrated in class. 
* You can get Python3 on http://www.python.org (use the latest python3 - 3.x)
* Submit your code/program as a .py file below.
* firstpartofyourstudentemail_labX.py
* Name it as "emfp_lab1.py" - Notice: the .py suffix (i.e. jdoe10_lab1.py)

#### A demo:
```
#!/usr/bin/env python3
# jdoe12_lab1.py
# date created 
# the frist part of your jdoe12@student email address 
# for example: jdoe12

# anything starting with a "hash" is considered a comment.

# Tradtional first program, (Used since the C programming book in 1978)

print("Hello, World!")
```

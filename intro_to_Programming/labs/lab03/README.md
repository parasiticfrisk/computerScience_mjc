# Lab 3

### Goal: Calculate Dollars and Cents from coins. 

#### Create a program that prompts a user for their name, and the number of coins they have. 
* What is your name? 
* How many quarters? 
* How many dimes? 
* How many nickels? 
* How many pennies? 

The output should say something like "Hello, NAME, you have XX dollars and YY cents in change." 

See Listing 3.4 ComputeChange.py in your Python book 
```
#!/usr/bin/env python3 
# coins.py 

usrname=input("What is your name -> ") 
dimes=int(input("Enter number of nickels -> ")) 
pennies=int(input("Enter number of pennies -> ")) 
total_cents=(dimes * 10)+(pennies) 
dollars=total_cents // 100 
cents=total_cents % 100 

print("Hello",usrname,"You have ",dollars," dollars and ",cents,"cents") 
```

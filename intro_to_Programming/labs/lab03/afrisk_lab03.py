#!/usr/bin/env python3
# afrisk_lab3.py
# calculate random coinage into dollars and cents

#DataEntry
username = input("What is your name -> ")
quarters = int(input("Enter number of quarters -> "))
dimes = int(input("Enter number of dimes -> "))
nickels = int(input("Enter number of nickels -> "))
pennies = int(input("Enter number of pennies -> "))

#Values
total_cents  = (quarters * 25) + (dimes * 10) + (nickels * 5) + (pennies)
dollars = total_cents // 100
cents = total_cents % 100

#Outcome
print("Hello" ,username,"You have ",dollars," dollars and ",cents,"cents")

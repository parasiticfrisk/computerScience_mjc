# Lab 5

### Goal: Completed Birth Test 

* Sequence - Get user data 
* Selection - Print correct paragraph 
* Iteration - first 4 digit sum to a single digit 

#### Iteration for Completed Birth Test: 
**Can you iterate over a string?** 
```
mylist=list("VeryOwnString") 
for counter1 in mylist: 
print(counter1) 
```
**Can you iterate over a number?**
```
mynumber=1234 
for counter2 in mynumber: 
print(counter2) 
```
**Convert number to a string:**
```
mynumber1=23456 
for counter3 in str(mynumber1) 
print(counter3) 
```
**Add an accumulator:**
```
accum1=0 
sum1=1998 
for x in str(sum1): 
accum1 = accum1 + int(x) 
print(x) 
```

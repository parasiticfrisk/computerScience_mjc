# Lab 8

### Goal: Refactor completed Birth Test to write to a logfile. 

* Where do these lines need to be placed? 
* Where there is a *print*, there is also a *file write*. 

```
file_handle_name=open("emfp_lab10.log",'a') 
line1("This is a line of text.") 

file_handle_name.write(line1) 
file_handle_name.close()
```

# Lab 2

### Goal: Data input for Birth Test and first calculation.
#### Write a program that collects the following data:

**(Input from the user)** 
* First name 
* Last name 
* Birth year 
* Birth month 
* Birth day 

**(output from your program)** 
* Line 1: First name, with a nice hello message. 
* Line 2: first sum = birth year + birth month + birth day, with a message about the first sum. 

#### Thoughts to consider:
* What of these values? 
* Strings? 
* Integers?

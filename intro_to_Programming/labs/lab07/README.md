# Lab 7

#### Goal: Refactor completed Birth Test to use a dictionary. 

* Where do these two lines need to be placed? 
* What is the result? 

```
color_dict = { 1:topcolor, 2:midcolor, 3:botcolor} 
print(color_dict[lightcolor]) 
```
```
#!/usr/bin/env python3 
# email_first_part_lab9.py 

topcolor = "Red Light - Please Stop." 
midcolor = "Yellow Light - Speed Up!" 
botcolor = "Green Light - Please Proceed." 

#lightcolor = int(input("Enter 1, 2 or 3 -> ")) 

if lightcolor == 1: 
print(topcolor) 
elif lightcolor == 2: 
print(midcolor) 
elif lightcolor == 3: 
print(botcolor) 
else: 
print("Unknown") 
```

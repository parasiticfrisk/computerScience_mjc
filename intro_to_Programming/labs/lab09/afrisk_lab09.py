#!/usr/bin/env python3
#afrisk_lab09
#10/23/2017

#Import Paras & Web
import afrisk_Paras as aP
import afrisk_Style as aS

#Personality Test
print("Welcome to the Birthdate Personality Calculator")
fname=input ("Please enter your name -> ")
print("Enter the following as the numerical value:")
bmo=int(input("Enter your birth month -> "))
bday=int(input("Enter your birth day -> "))
byr=int(input("Enter your birth year -> "))
#Calculate Para Num & Year Num
sum1=bday+byr+bmo
accum=(sum1%9)
cyear=(accum%12)
print("Year of the "+aP.chyr[cyear]+'\n'+aP.result[accum])

#Create WebPage
filenameW=("/home/afrisk/public_html/PersonalityTest.html")
fhandle=open(filenameW,'w')
fhandle.write(aS.hdr+aS.css+aS.head+fname+aS.head1+aS.ppara+fname+aS.ppara1+aP.result[accum]+aS.zodiac+aP.chyr[cyear]+aS.zodiac1+aS.foot+aS.ftr)
fhandle.close()

#Logfile
filename=("afrisk_lab09.log\n")
fhandle=open(filename,'a')
fhandle.write(fname+str(bday)+str(bmo)+str(byr)+aP.result[accum]+aP.chyr[cyear])
fhandle.close()

# Lab 9

### Goal: File i/o to create a webpage 

* Sequence - Get user data 
* Selection - Print correct paragraph 
* Iteration - first 4 digit sum to a single digit 
* Completed Birth Test 
* Refactor completed birth test to replace if/elif/else with a dictionary 
* Logfile - Add a logfile to your completed code 
* Web Page - Add a web page to your completed code 

#### Demo code - How to write to a webpage: 
```
#!/usr/bin/env python3 
# emfp_lab11.py 

import webbrowser 

teststuff=''' 
The quick brown fox jumps over the lazy dog. 
The grumpy wizards make a toxic brew for the evil queen and jack. 
''' 
tophtml=''' <!DOCTYPE html> <html> <head><meta charset="UTF-8"> 
<title>Birth Test</title> </head> <body> <h2> <pre> ''' 

bthtml=''' </h2> </pre> </body> </html> ''' 
# notice the 'w' instead of 'a' what is the difference? 
fhw=open("basic5.html",'w') 
fhw.write(tophtml) 
fhw.write(teststuff) 
fhw.write(bthtml) 
fhw.close() 

webbrowser.open_new("basic5.html") 
```

# Lab 6

### Goal: Learn SQL at [w3schools](https://www.w3schools.com/)
* Screen shot of sql quiz 80% or better 
* Get the Firefox Sqlite Manager
* Right Click -> Save As ... HenryBooks sqlite3 database
* Open the Sqlite Manager and Connect to the downloaded HenryBooks.sqlite database 

#### Answer the questions with the correct sql statements:
**Henry Books - State the correct SQL statement.** 

0. List the complete Author table (all rows and all columns).
1. List the author number and last name for every author.
2. List the complete Branch table (all rows and all columns).
3. List the name of every publisher located in Boston.
4. List the name of every publisher not located in Boston.
5. List the name of every branch that has at least nine employees.
6. List the book code and title of every book that has the type SFI.
7. List the book code and title of every book that has the type SFI and that is a paperback.
8. List the book code and title of every book that has the type SFI or that has the publisher code PE.
9. List the book code, title, and price for each book with a price that is greater than or equal to $5 but less than or equal to $10.
10. List the book code and title of every book that has the type FIC and a price of less than $10.

**_Submit a document with your sql statements and the screen shot via Canvas_**

#!/usr/bin/env python3
#afrisk_final.py
#270 final project

import webbrowser     #webpage output
from turtle import *  #gui output
from math import *    #biorythm calculations
import datetime       #biorythm calculations
import sqlite3        #database log

#Imported Dictionaries & Webpage structure
import afrisk_Dictionaries as aDict
import afrisk_webPage as wPage
#JavaScript Option
#import webPage as wPage

#Set up turtle for output
bgcolor('#102129')
bgpic('Final.gif')
setup(width = 1280, height = 800)
color('White')
penup()
goto(0,-100)

#Get user informtion
usrname = textinput('Name','Please enter your name')
bmonth = numinput('Birth Month','Enter your birth month',\
                  1, minval = 1, maxval = 12)
bday = numinput('Birth Day','Enter your birth day',\
                1, minval = 1, maxval = 31)
byear = numinput('Birth Year','Enter your birth year',\
                 1997, minval = 1900, maxval = 2020)

#Calculate personality paragraph
def getPersonalityParagraph(para):
    return sum([int(i) for i in str(para)])
para = int(bmonth + bday + byear)

#Return birth sum back to the funtion if greater than single digit
while para > 9: para = getPersonalityParagraph(para)

#Calculate Chinese Zodiac
def getChineseZodiac(year):
    return sum([int(i) for i in str(year)])
year = int(bmonth + bday + byear)-1899

#Return birth sum back to function if greater than 12
while year > 12: year = getChineseZodiac(year)

#Calculate western astrology
if bmonth == 12:
    sign = 9 if(bday < 22) else 10
elif bmonth == 1:
    sign = 10 if(bday < 20) else 11
elif bmonth == 2:
    sign = 11 if(bday < 19) else 12
elif bmonth == 3:
    sign = 12 if(bday < 21) else 1
elif bmonth == 4:
    sign = 1 if(bday < 20) else 2
elif bmonth == 5:
    sign = 2 if(bday < 21) else 3
elif bmonth == 6:
    sign = 3 if(bday < 21) else 4
elif bmonth == 7:
    sign = 4 if(bday < 23) else 5
elif bmonth == 8:
    sign = 5 if(bday < 23) else 6
elif bmonth == 9:
    sign = 6 if(bday < 23) else 7
elif bmonth == 10:
    sign = 7 if(bday < 23) else 8
elif bmonth == 11:
    sign = 8 if(bday < 22) else 9

#Biorythm information
dob = datetime.date(int(byear), int(bmonth), int(bday))
today = datetime.date.today()
daysAlive = (today - dob).days

#Calculate biorythm output
bioP = sin((2* pi * daysAlive)/23) * 100
phys = int(round(bioP))
bioE = sin((2* pi * daysAlive)/28) * 100
emot = int(round(bioE))
bioI = sin((2* pi * daysAlive)/33) * 100
intl = int(round(bioI))

#Turtle GUI output
line1 =('Hello, '+ usrname +'!\n'+ aDict.ppara[para]+'\n'+'\n'\
       +aDict.cyear[year]+'\n'+'\n'+ aDict.asign[sign]+'\n'+'\n'\
        +'Phys: '+str(phys)+'  Emot: '+str(emot)+'  Intl: '+str(intl))
write(line1, False, align ='center', font =('Arial',10,'bold'))
hideturtle()
done()


#Generate & open rewritable webpage
filenameW =('finalProject.html')
fhandle = open(filenameW,'w')
fhandle.write(wPage.css + wPage.headOpen + usrname + wPage.headClose + \
              wPage.paraOpen + aDict.ppara[para] + wPage.bioOpen +\
              'Phys: ' + str(phys) + '  Emot: ' + str(emot) + '  Intl: ' + \
              str(intl) + wPage.bioClose + wPage.westOpen + aDict.asign[sign] \
              + wPage.chinOpen + aDict.cyear[year] + wPage.chinClose + \
              wPage.foot)
'''JavaScript Option
fhandle.write(wPage.scriptOpen + usrname + wPage.scriptUser + aDict.ppara[para]\
              + wPage.scriptPara + 'Phys: ' + str(phys) + '  Emot: ' + str(emot)\
              + '  Intl: ' + str(intl) + wPage.scriptBio + aDict.asign[sign] \
              + wPage.scriptWest + aDict.cyear[year] + wPage.scriptChin)'''
fhandle.close()

#Open web page in new tab
webbrowser.open_new_tab('finalProject.html')


#SQLite database & Log File
#Connect to database
con = sqlite3.connect('afrisk_final.sqlite')
c = con.cursor()

#Create new table
c.execute('''CREATE TABLE IF NOT EXISTS users (name text, birthmonth integer,\
birthday integer, birthyear integer, personality integer, zodiac integer,\
astrology integer, biorythmPhys integer, biorythmEmot integer, biorythmIntl \
integer)''')

#Insert data into table
c.execute('''INSERT INTO users(name, birthmonth, birthday, birthyear, \
personality, zodiac, astrology, biorythmPhys, biorythmEmot, biorythmIntl) \
VALUES ('usrname','bmonth','bday','byear','para','year','sign','phys','emot',\
'intl')''')
con.commit()

#Generate appending log file
filename = ('afrisk_final.log')
fhandle = open(filename,'a')
#User input & dictionary numbers
fhandle.write(usrname+' '+str(bmonth)+' '+str(bday)+' '+str(byear)+'\n'+\
              '\n'+aDict.ppara[para]+'\n'+'\n'+aDict.cyear[year]+'\n'+str(phys)\
              +' '+str(emot)+' '+str(intl)+'\n'+'\n')
fhandle.close()

#!/usr/bin/env Python3

'''
Webpage style and structure using CSS
Coded by August Frisk (c) 2017
Bonus: use CSS in webpage
Bonus: add western astrology
Bonus: add biorythm information
'''


css = ("""<!DOCTYPE html><html><head><title>Personality Test</title><style>
*{margin: 0;padding: 0;}
body {font-family: "Handlee", cursive; font-size: 13pt;
    background-color: #222;padding: 10px;margin: 0;}
h1 {font-size: 15pt; color: #20bcd5; text-align: center;
    padding: 18px 0 18px 0; margin: 0 0 10px 0;}
h1 span {border: 4px dashed #20bcd5; padding: 10px;}
p {padding: 0;margin: 0;}
.img-circle {border: 3px solid white;border-radius: 50%;}
.section {background-color: #fff;padding: 15px;margin-bottom: 10px;
    border-radius: 10px;}
#header {background-image: url("http://static.guim.co.uk/sys-images/Environment/Pix/columnists/2012/2/7/1328628184434/Carina-Nebula-from-ESOs-V-011.jpg");
    background-size: cover;}
#header img {display: block; width: 80px; height: 80px; margin: auto;}
#header p {font-size: 25pt; color: #ffffff; padding-top: 5px; margin: 0;
    font-weight: bold; text-align: center;}
.quote {font-size: 12pt; text-align: right; margin-top: 10px;}
.copyright {font-size: 8pt; text-align: right; padding-bottom: 10px;
    color: #fff;}
</style></head><body>""")
# HTML open with CSS

headOpen =('<div id="header" class="section"><img alt="" class="img-circle" src="https://www.iconexperience.com/_img/o_collection_png/green_dark_grey/512x512/plain/astronaut.png"><p>')
headClose =('</p></div>')
# Webpage header with username


paraOpen =('<div class="section"><h1><span>Personality Type</span></h1><p>')
# Personality Paragraph
bioOpen =('</p><p class="quote">')
bioClose =('</p></div>')
# Biorythm Percentages in lower right corner


westOpen =('<div class="section"><h1><span>Astrological Signs</h1><p>')
# Western Astrology
chinOpen = ('</p><p class="quote">')
chinClose =('</p></div>')
# Chinese Zodiac in lower right corner


foot=('<div class="copyright"> &copy; 2017 August Frisk. All rights reserved. </div></body></html>')
# Copyright and close

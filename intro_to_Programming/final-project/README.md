# Lab 11
### Final Project:
* **Grade C:** Use completed birth test program, write appended logfile, write to new webpage, turtle gui, import for paragraphs 
* **Grade B:** Present code to class, CSS for the webpage 
* **Grade A:** Refactor code to use functions, eye-candy for gui 
* **Bonus +:** Add western astrology to display, logfile, & wepage 
* **Bonus ++:** Add biorythm calculations to display, logfile, & webpage 
* **Bonus +++:** Write logfile information to sqlite3 database

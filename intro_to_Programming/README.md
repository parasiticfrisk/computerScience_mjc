# Introduction to Programming (CSCI 270)
Python labs and final project from my Introduction to Programming course at MJC. I plan on continuing to come back to these labs to change them in various ways or use as a reference.

### Course Description:
First course in computer compliant with the standards of the Association for Computing Machinery (ACM). This course is for students with little or no programming experience. General computer literacy issues useful for technicians such as computer hardware, software development, operating systems, and telecommunications. Beggining problem-solving analysis, documentation, algorithm design, control structures, as well as coding using an appropriate beginning language. Data manipulation, logic, looping, program testing, and program maintenance will be stressed.
